var Header = {
  controller: function() {

  },
  view: function(ctrl) {
    return m('div', [
      m('nav', { class: 'navbar navbar-default navbar-fixed-top' }, [
        m('div', { class: 'container' }, [
          m('div', { class: 'navbar-header' }, [
            m('a', { class: 'navbar-brand', href: '#' }, 'Cryptocharts')
          ])
        ])
      ])
    ]);
  }
};