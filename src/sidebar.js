var Sidebar = {
  controller: function () {

  },
  view: function (ctrl) {
    return m('div', {class: 'col-md-3', style: "margin-top: 20px"}, [
      m('div', {class: 'list-group'}, [
        m('a', {class: 'list-group-item', href: '#'}, 'Bitcoin'),
        m('a', {class: 'list-group-item', href: '#'}, 'Ripple'),
        m('a', {class: 'list-group-item', href: '#'}, 'Litecoin'),
        m('a', {class: 'list-group-item', href: '#'}, 'Dash'),
        m('a', {class: 'list-group-item', href: '#'}, 'Stellar'),
        m('a', {class: 'list-group-item', href: '#'}, 'BitShares'),
      ])
    ])
  }
};

