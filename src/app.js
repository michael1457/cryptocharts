var App = {
  controller: function() {

  },
  view: function(ctrl) {
    return m('div', [
      m.component(Header),
      m('div', {class: 'container'}, [
        m('div', [
          m.component(Chart),
          m.component(Sidebar)
        ]),
        m.component(BottomBar)
      ])
    ]);
  }
};

m.mount(document.getElementById('app'), App);